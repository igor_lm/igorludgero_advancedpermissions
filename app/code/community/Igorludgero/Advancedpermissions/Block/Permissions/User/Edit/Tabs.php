<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 25/08/16
 * Time: 13:10
 */

class Igorludgero_Advancedpermissions_Block_Permissions_User_Edit_Tabs extends Mage_Adminhtml_Block_Permissions_User_Edit_Tabs
{

    public function __construct()
    {
        parent::__construct();
    }

    protected function _beforeToHtml()
    {

        $helperAdvanced = Mage::helper("igorludgero_advancedpermissions");

        $adminId = $this->getRequest()->getParam('user_id');

        $this->addTab('time_section', array(
            'label'     => $helperAdvanced->__('Day/Hour Permission'),
            'title'     => $helperAdvanced->__('Day/Hour Permission'),
            'content'   => $this->getLayout()->createBlock('advancedpermissions/adminhtml_datetime')->setData('admin_id',$adminId)->toHtml(),
        ));

        return parent::_beforeToHtml();
    }

}