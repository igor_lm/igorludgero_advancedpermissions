<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 26/08/16
 * Time: 13:57
 */

class Igorludgero_Advancedpermissions_Block_Adminhtml_Datetime_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {
        $this->_blockGroup = 'advancedpermissions';
        $this->_controller = 'adminhtml_datetime';

        parent::__construct();

        $this->_updateButton('save', 'label', $this->__('Save Permission'));
        $this->_updateButton('delete', 'label', $this->__('Delete Permission'));
    }

    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if(Mage::registry('advancedpermissions')!=null) {
            return $this->__('Edit Day/Hour Permission');
        }
        else {
            return $this->__('New Day/Hour Permission');
        }
    }

    protected function _prepareLayout()
    {
        $this->_removeButton('back');
        $this->_removeButton('reset');

        return parent::_prepareLayout();
    }

}