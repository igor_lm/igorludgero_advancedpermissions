<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 25/08/16
 * Time: 23:24
 */

class Igorludgero_Advancedpermissions_Block_Adminhtml_Datetime extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'advancedpermissions';
        $this->_controller = 'adminhtml_datetime';
        $this->_headerText = Mage::helper('igorludgero_advancedpermissions')->__('User day and hours permission');
        $this->_addButton('add_newPermission', array(
            'label'   => Mage::helper('igorludgero_advancedpermissions')->__('Add New Permission'),
            'onclick' => "setLocation('{$this->getUrl('*/datetime/edit/admin_id/'.$this->getRequest()->getParam('user_id'))}')",
            'class'   => 'add'
        ));
        parent::__construct();
        $this->_removeButton('add');
    }

}