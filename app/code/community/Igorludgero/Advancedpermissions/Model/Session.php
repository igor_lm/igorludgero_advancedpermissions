<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 27/08/16
 * Time: 01:08
 */

class Igorludgero_Advancedpermissions_Model_Session extends Mage_Admin_Model_Session{

    public function validate()
    {
        if (!isset($this->_data[self::VALIDATOR_KEY])) {
            $this->_data[self::VALIDATOR_KEY] = $this->getValidatorData();
        }
        else {
            if (!$this->_validate()) {
                $this->getCookie()->delete(session_name());
                throw new Mage_Core_Model_Session_Exception('');
            }
            else{
                $helperPermissions = Mage::helper("igorludgero_advancedpermissions");
                if(isset($this->_data["user"])){
                    $user = $this->_data["user"];
                    if($user->getUserId()!=null) {
                        $userId = $user->getUserId();
                        if($helperPermissions->canLog($userId)==false) {
                            $this->getCookie()->delete(session_name());
                        }
                    }
                }
            }
        }

        return $this;
    }

}