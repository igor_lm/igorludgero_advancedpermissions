<?php
/**
 * Created by PhpStorm.
 * User: igorludgeromiura
 * Date: 27/08/16
 * Time: 00:18
 */

class Igorludgero_Advancedpermissions_Model_User extends Mage_Admin_Model_User {

    /** Authenticate function with day/time verification */
    public function authenticate($username, $password)
    {
        $helperPermissions = Mage::helper("igorludgero_advancedpermissions");

        $config = Mage::getStoreConfigFlag('admin/security/use_case_sensitive_login');
        $result = false;

        try {
            Mage::dispatchEvent('admin_user_authenticate_before', array(
                'username' => $username,
                'user'     => $this
            ));
            $this->loadByUsername($username);
            $sensitive = ($config) ? $username == $this->getUsername() : true;

            if ($sensitive && $this->getId() && Mage::helper('core')->validateHash($password, $this->getPassword())) {
                if ($this->getIsActive() != '1') {
                    Mage::throwException(Mage::helper('adminhtml')->__('This account is inactive.'));
                }
                if (!$this->hasAssigned2Role($this->getId())) {
                    Mage::throwException(Mage::helper('adminhtml')->__('Access denied.'));
                }
                if($helperPermissions->canLog($this->getId())==false){
                    Mage::throwException($helperPermissions->__('Access denied in this day and time. Please ask to the administrator to add the permission.'));
                }
                $result = true;
            }

            Mage::dispatchEvent('admin_user_authenticate_after', array(
                'username' => $username,
                'password' => $password,
                'user'     => $this,
                'result'   => $result,
            ));
        }
        catch (Mage_Core_Exception $e) {
            $this->unsetData();
            throw $e;
        }

        if (!$result) {
            $this->unsetData();
        }
        return $result;
    }

}